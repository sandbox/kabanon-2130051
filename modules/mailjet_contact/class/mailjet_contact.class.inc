<?php
/**
 * @file 
 * The Mailjet Contact Entity class.
 */
class MailjetContactEntity extends Entity {


  /**
   * Creates a new Mailjet Contact Entity.
   *
   * @see entity_create()
   */
  public function __construct(array $values = array()) {
    parent::__construct($values, 'mailjet_contact');
  }
  /**
   * Override this in order to implement a custom default URI.
   */
  protected function defaultUri() {
    return array('path' => 'mailjet_contact/' . $this->identifier());
  }
  /**
   * Save Mailjet 
   */
  public function save($update_on_mailjet = TRUE) {
    return entity_get_controller($this->entityType)->save($this, NULL, $update_on_mailjet);
  }
}
/**
 * Our custom controller for the mailjet_contacts type.
 *
 * We're choosing to extend the controller provided by the entity module so that
 * we'll have full CRUD support for mailjet_contactsentities.
 */
class MailjetContactEntityController extends EntityAPIController {
  /**
   * Implements EntityAPIControllerInterface.
   *
   * @param $content
   *   Optionally. Allows pre-populating the built content to ease overridding
   *   this method.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $build = parent::buildContent($entity, $view_mode, $langcode, $content);
    
    return $build;
  }
  /**
   * Implements EntityAPIControllerInterface.
   *
   * @param $transaction
   *   Optionally a DatabaseTransaction object to use. Allows overrides to pass
   *   in their transaction object.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL, $update_on_mailjet = TRUE) {

    $return = parent::save($entity, $transaction);

    if ($return && $update_on_mailjet) {
      // We assume that $update_on_mailjet is only egal to FALSE when there is a
      // synchronisation process
      $_op = ($op == 'created') ? 'created' : 'updates';
      drupal_set_message(t('Mailjet Contact successfully ' . $_op));
    }
    return $return;
  }
  /**
   * Implements EntityAPIControllerInterface.
   *
   * @param $transaction
   *   Optionally a DatabaseTransaction object to use. Allows overrides to pass
   *   in their transaction object.
   */
  public function delete($ids, DatabaseTransaction $transaction = NULL) {

  }
}

/**
 * Our custom controller for the admin ui.
 */
class MailjetContactEntityUIController extends EntityDefaultUIController {
  
}
