<?php
/**
 * @file
 * Admin pages for mailjet_list.module.
 */

/**
 * Form for adding or editing a mailjet_list entity.
 */
function mailjet_list_form($form, &$form_state, $mailjet_list_entity, $op = 'edit') {    

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => isset($mailjet_list_entity->label) ? $mailjet_list_entity->label : '',
    '#description' => t('Title of your list (max length 60 characters)'),
    '#required' => TRUE,
  );
  
  $form['name'] = array(
    '#title' => t('Name'),
    '#type' => 'textfield',
    '#default_value' => isset($mailjet_list_entity->name) ? $mailjet_list_entity->name : '',
    '#description' => t('Mailjet\'s contact list internal name (must be unique, may only contain alpha-numeric characters)'),
    '#required' => TRUE,
  );
  $cancel_link = (isset($_GET['destination'])) ? $_GET['destination'] : 'admin/mailjet/list';
  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => isset($mailjet_list_entity->lid) ? t('Update Mailjet list') : t('Save Mailjet list'),
    ),
    'cancel' => array(
      '#markup' => l(t('Cancel'), $cancel_link),
    ),
  );  
  return $form;
}

/**
 * Validation form entity add/edit form.
 */
function mailjet_list_form_validate($form, &$form_state) {
  if (strlen($form_state['values']['label']) > 60) {
    form_set_error('label', t('The title of your list may not be greater than 60 characters.'));
  }
  if (!ctype_alnum($form_state['values']['name'])) {
    form_set_error('name', t('The name of your list may only contain alpha-numeric characters.'));
  }
  // Name have to be unique.
  $query = new EntityFieldQuery();
  $entities = $query->entityCondition('entity_type', 'mailjet_list')
                    ->propertyCondition('name', $form_state['values']['name']);
  $result = $query->execute();
  if (!empty($result)) {
    form_set_error('name', t('The name may be unique.'));
  }
}

/**
 * Submit handler for entity add/edit form.
 */
function mailjet_list_form_submit($form, &$form_state) {
  
  $mailjet_list_asset = entity_ui_form_submit_build_entity($form, $form_state);
  $mailjet_list_asset->save(TRUE);
  
  $form_state['redirect'] = 'admin/mailjet/list';
}


function mailjet_list_sync() {
  
  // 
  $mj = new mailjet_integration();
  $params = array(
    'limit' => 10,
    //'orderby' => 'id ASC',
    //'start' => 0,
  );
  
  $response = $mj->listsAll($params);
  if ($response && $response->status == 'OK') {
    $operations = array();
    foreach ($response->lists as $key => $list) {
      $operations[] = array('mailjet_list_sync_op', array($list));
    }
    
    if (!empty($operations)) {
      $batch = array(
        'operations' => $operations,
        'finished' => 'mailjet_list_sync_op_finished',
        'title' => t('Processing synchronization Mailjet list'),
        'init_message' => t('Sync Mailjet list is starting.'),
        'progress_message' => t('Processed @current out of @total.'),
        'error_message' => t('Sync Mailjet list has encountered an error.'),
        'file' => drupal_get_path('module', 'mailjet_list') . '/inc/mailjet_list.admin.inc',
      );
      batch_set($batch);
      batch_process('admin/mailjet/list');
    }
    else {
      drupal_set_message(t('There is no list to import'), 'warning');
      drupal_goto('admin/mailjet/list');
    }
  }
  else {
    drupal_set_message(t('Can not synchronize Mailjet Campains : Network error'), 'error');
    drupal_goto('admin/mailjet/list');
  }
}

function mailjet_list_sync_op($list, &$context) {
  
  $mailjet_list = entity_load('mailjet_list', array($list->id));
  
  if (!empty($mailjet_list)) {
    // update entity
    $vars = get_object_vars($list);
    foreach ($vars as $key => $value) {
      $mailjet_list[$list->id]->$key = $value;
    }
    $mailjet_list[$list->id]->save(FALSE);
    $context['results']['update'] = (isset($context['results']['update'])) ? $context['results']['update'] + 1 : 1;
  }
  else {
    // create entity
    $data = array();
    $vars = get_object_vars($list);
    foreach ($vars as $key => $value) {
      $data[$key] = $value;
    }
    $data['lid'] = $data['id'];
    
    $mailjet_list = entity_create('mailjet_list', $data);       
    if ($mailjet_list->save(FALSE)) {
      $context['results']['insert'] = (isset($context['results']['insert'])) ? $context['results']['insert'] + 1 : 1;        
    }
    else {
      drupal_set_message(t('Can not create Mailjet list', 'error'));
    }
  }
}

function mailjet_list_sync_op_finished($success, $results, $operations) {
  if ($success) {
    if (isset($results['insert'])) {
      drupal_set_message(format_plural($results['insert'], '1 list have been created', '@count lists have been created'));
    }
    if (isset($results['update'])) {
      drupal_set_message(format_plural($results['update'], '1 list have been updated', '@count lists have been updated'));
    }
    drupal_set_message(t('Sync Mailjet list finished'));
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)));
    drupal_set_message($message, 'error');
  }
}