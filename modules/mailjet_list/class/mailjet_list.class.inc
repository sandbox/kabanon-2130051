<?php
/**
 * @file 
 * The Mailjet List Entity class.
 */
class MailjetListEntity extends Entity {


  /**
   * Creates a new Mailjet List Entity.
   *
   * @see entity_create()
   */
  public function __construct(array $values = array()) {
    parent::__construct($values, 'mailjet_list');
  }
  /**
   * Override this in order to implement a custom default URI.
   */
  protected function defaultUri() {
    return array('path' => 'mailjet_list/' . $this->identifier());
  }
  /**
   * Save Mailjet 
   */
  public function save($update_on_mailjet = TRUE) {
    return entity_get_controller($this->entityType)->save($this, NULL, $update_on_mailjet);
  }
}
/**
 * Our custom controller for the mailjet_lists type.
 *
 * We're choosing to extend the controller provided by the entity module so that
 * we'll have full CRUD support for mailjet_listsentities.
 */
class MailjetListEntityController extends EntityAPIController {
  /**
   * Implements EntityAPIControllerInterface.
   *
   * @param $content
   *   Optionally. Allows pre-populating the built content to ease overridding
   *   this method.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $build = parent::buildContent($entity, $view_mode, $langcode, $content);
    
    $build['label'] = array(
      '#theme' => 'field',
      '#weight' => 0,
      '#title' => t('Label'),
      '#access' => TRUE,
      '#label_display' => 'inline',
      '#field_name' => 'label',
      '#field_type' => 'text',
      '#entity_type' => 'mailjet_list',
      '#bundle' => 'mailjet_list',
      '#items' => array(array('value' => $entity->label)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $entity->label),
    );
    $build['email'] = array(
      '#theme' => 'field',
      '#weight' => 0,
      '#title' => t('Email'),
      '#access' => TRUE,
      '#label_display' => 'inline',
      '#field_name' => 'lid',
      '#field_type' => 'text',
      '#entity_type' => 'mailjet_list',
      '#bundle' => 'mailjet_list',
      '#items' => array(array('value' => $entity->lid . '@lists.mailjet.com')),
      '#formatter' => 'text_default',
      0 => array('#markup' => $entity->lid . '@lists.mailjet.com'),
    );
    $build['created_at'] = array(
      '#theme' => 'field',
      '#weight' => 0,
      '#title' => t('Created at'),
      '#access' => TRUE,
      '#label_display' => 'inline',
      '#field_name' => 'field_created_at',
      '#field_type' => 'text',
      '#entity_type' => 'mailjet_list',
      '#bundle' => 'mailjet_list',
      '#items' => array(array('value' => $entity->created_at)),
      '#formatter' => 'text_default',
      0 => array('#markup' => format_date($entity->created_at)),
    );
    
    
    return $build;
  }
  /**
   * Implements EntityAPIControllerInterface.
   *
   * @param $transaction
   *   Optionally a DatabaseTransaction object to use. Allows overrides to pass
   *   in their transaction object.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL, $update_on_mailjet = TRUE) {
    $return = FALSE;
    $save   = FALSE;

    $op = (isset($entity->is_new)) ? 'create' : 'update';
    // Do we have to call Mailjet API ?
    if ($update_on_mailjet) {
      // Insert action.
      if ($op == 'create' && $this->check_values($entity)) {
        $_mailjet_params = array(
          'method' => 'POST',
          'label' => $entity->label,
          'name' => $entity->name,
        );
        // Mailjet API call.
        $mj = new mailjet_integration();
        $response = $mj->listsCreate($_mailjet_params);
        // Check response status.
        if (isset($response->status) && $response->status == 'OK') {
          $entity->lid = $response->list_id;
          $save = TRUE;
        }
      }
      // Update action.
      elseif ($op == 'update' && $this->check_values($entity, 'update')) {
        $_mailjet_params = array(
          'method' => 'POST',
          'id' => $entity->lid,
        );
        if (isset($entity->label)) {
          $_mailjet_params['label'] = $entity->label;
        }
        if (isset($entity->name)) {
          $_mailjet_params['name'] = $entity->name;
        }        
        // Mailjet API call.
        $mj = new mailjet_integration();
        $response = $mj->listsUpdate($_mailjet_params);
        // Check response status.
        if (isset($response->status) && $response->status == 'OK') {
          $save = TRUE;
        }
      }
    }
    else {
      $save = TRUE;
    }
    // Now we can save entity in database.
    if ($save) {
      $return = parent::save($entity, $transaction);
    }
    if ($return && $update_on_mailjet) {
      // We assume that $update_on_mailjet is only egal to FALSE when there is a
      // synchronisation process
      $_op = ($op == 'created') ? 'created' : 'updates';
      drupal_set_message(t('Mailjet List successfully ' . $_op));
    }

    return $return;
  }
  /**
   * Implements EntityAPIControllerInterface.
   *
   * @param $transaction
   *   Optionally a DatabaseTransaction object to use. Allows overrides to pass
   *   in their transaction object.
   */
  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    foreach ($ids as $lid) {
      $_mailjet_params = array(
        'method' => 'POST',
        'id' => $lid,
      );
      $mj = new mailjet_integration();
      $response = $mj->listsDelete($_mailjet_params);
      if (!$response) {
        // Remove from ids array to prevent delete action
        $key = array_search($lid, $ids);
        unset($ids[$key]);
      }    
    }
    return parent::delete($ids, $transaction);
  }
  /**
   * Check required values.
   * 
   * @param object $entity
   *   The entity to check.
   * @param string $action
   *   The action. Insert by default.
   * 
   * @return bool
   *   Autorization to perform APIController method.
   */
  protected function check_values(&$entity, $action = 'insert') {
    $return = TRUE;
    if ($action == 'insert') {
      if (!isset($entity->label)) {
        $return = FALSE;
        drupal_set_message(t('%key properties is required', array('%key' => 'label')));
      }
      elseif (!$this->check_label($entity->label)) {
        $return = FALSE;
      }
      if (!$this->check_name($entity)) {
        $return = FALSE;
      }
    }
    elseif ($action == 'update') {
      if (!isset($entity->lid)) {
        $return = FALSE;
        drupal_set_message(t('%key properties is required', array('%key' => 'lid')));
      }   
    }
    // Sanitize values.
    if ($return) {
      $this->sanitize_values($entity);
    }
    return $return;
  }
  /**
   * Check label property.
   */
  public function check_label($entity, $error = 'screen', $form_input = 'label') {
    if (isset($entity->label) && strlen($entity->label) > 60) {
      switch ($error) {
        case 'screen' :
          drupal_set_message(t('%key properties is can have more than %caracters caracters.', array('%key' => 'name', '%caracters' => 60)), 'error');
          break;
        case 'form' :
          form_set_error($form_input, t('The title of your list may not be greater than 60 characters.'));
          break;
      }
      return FALSE;
    }
    return TRUE;
  }
  /**
   * Check name property.
   */
  public function check_name($entity, $error = 'screen', $form_input = 'name') {
    // Name value is required
    if (!isset($entity->name)) {
      drupal_set_message(t('%key properties is required', array('%key' => 'name')));
      return FALSE;
    }
    else {
      // Value must be lower than 255 caracters. 
      if (strlen($entity->name) > 255) {
        switch ($error) {
          case 'screen' :
            drupal_set_message(t('%key properties is can have more than %caracters caracters.', array('%key' => 'name', '%caracters' => 255)), 'error');
            break;
          case 'form' :
            form_set_error($form_input, t('The name of your list may only contain alpha-numeric characters.'));
            break;
        }
        return FALSE;
      }
      // Value must be alpha numeric.
      if (!ctype_alnum($entity->name)) {
        switch ($error) {
          case 'screen' :
            drupal_set_message(t('%key properties may only contain alpha-numeric characters.', array('%key' => 'name')), 'error');
            break;
          case 'form' :
            form_set_error($form_input, t('The name of your list may only contain alpha-numeric characters.'));
            break;
        }
        return FALSE;
      }
      // Value may be unique.
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'mailjet_list')
            ->propertyCondition('name', $entity->name);
      if (!empty($entity->lid)) {
        $query->propertyCondition('lid', $entity->lid, '!=');    
      }
      $result = $query->execute();
      if (!empty($result)) {
        switch ($error) {
          case 'screen' :
            drupal_set_message(t('%key properties may be unique.', array('%key' => 'name')), 'error');
            break;
          case 'form' :
            form_set_error($form_input, t('The name may be unique.'));
            break;
        }        
      }
    }
    
    return TRUE;
  }
  /**
   * Sanitize values.
   */
  protected function sanitize_values(&$entity) {
    // no HTML tags allowed for label
    if (isset($entity->label)) {
      $entity->label = strip_tags($entity->label);
    }
  }
}

/**
 * Our custom controller for the admin ui.
 */
class MailjetListEntityUIController extends EntityDefaultUIController {
  
}
