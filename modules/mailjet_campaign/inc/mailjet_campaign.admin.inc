<?php
/**
 * @file
 * Admin pages for mailjet_campaign.module.
 */

/**
 * Form for adding or editing a mailjet_campaign entity.
 */
function mailjet_campaign_form($form, &$form_state, $mailjet_campaign_entity, $op = 'edit') {  
    
  $form['sender_email'] = array(
    '#title' => t('Sender email'),
    '#type' => 'textfield',
    '#default_value' => isset($mailjet_campaign_entity->sender_email) ? $mailjet_campaign_entity->sender_email : '',
    '#description' => t('The campaign sender\'s email'),
    '#required' => TRUE,
  );
  
  $form['sender_name'] = array(
    '#title' => t('Sender name'),
    '#type' => 'textfield',
    '#default_value' => isset($mailjet_campaign_entity->sender_name) ? $mailjet_campaign_entity->sender_name : '',
    '#description' => t('The campaign sender\'s name'),
    '#required' => TRUE,
  );
  
  $form['reply_to'] = array(
    '#title' => t('Reply to'),
    '#type' => 'textfield',
    '#default_value' => isset($mailjet_campaign_entity->reply_to) ? $mailjet_campaign_entity->reply_to : '',
    '#description' => t('The campaign reply to email'),
  );
  
  $form['subject'] = array(
    '#title' => t('Subject'),
    '#type' => 'textfield',
    '#default_value' => isset($mailjet_campaign_entity->subject) ? $mailjet_campaign_entity->subject : '',
    '#description' => t('The campaign subject'),
    '#required' => TRUE,
  );
  
  $form['title'] = array(
    '#title' => t('Title'),
    '#type' => 'textfield',
    '#default_value' => isset($mailjet_campaign_entity->title) ? $mailjet_campaign_entity->title : '',
    '#description' => t('The campaign title'),
  );

  $form['locale'] = array(
    '#title' => t('Locale'),
    '#type' => 'select',
    '#options' => array(
      'en' => 'en',  
      'fr' => 'fr',
      'de' => 'de',
      'it' => 'it',
      'es' => 'es',
      'nl' => 'nl',
    ),
    '#default_value' => isset($mailjet_campaign_entity->locale) ? $mailjet_campaign_entity->locale : 'en',
    '#required' => TRUE,
  );
  
  $form['footer'] = array(
    '#title' => t('Footer'),
    '#type' => 'select',
    '#options' => array(
      'default' => 'default',  
      'none' => 'none',
    ),
    '#default_value' => isset($mailjet_campaign_entity->footer) ? $mailjet_campaign_entity->footer : 'default',
    '#required' => TRUE,
  );

  $options = array(
    'draft' => t('Draft'), 
  );
  
  if (isset($mailjet_campaign_entity->cid)) {
    $options['archived'] = t('Archived');
    $options[''] = t('Deleted');
  }

  $form['status'] = array(
    '#title' => t('Status'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => isset($mailjet_campaign_entity->status) ? $mailjet_campaign_entity->status : 'draft',
  );
  
  
  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => isset($mailjet_campaign_entity->cid) ? t('Update Mailjet campaign') : t('Save Mailjet campaign'),
    ),
  );
  
  return $form;
}

/**
 * Validation form entity add/edit form.
 */
function mailjet_campaign_form_validate($form, &$form_state) {
  if (!filter_var($form_state['values']['sender_email'], FILTER_VALIDATE_EMAIL)) {
    form_set_error('sender_email', t('The sender email format is not valid'));
  }
  
  if (!empty($form_state['values']['reply_to']) && !filter_var($form_state['values']['reply_to'], FILTER_VALIDATE_EMAIL)) {
    form_set_error('reply_to', t('The reply to email format is not valid'));
  }
}

/**
 * Submit handler for entity add/edit form.
 */
function mailjet_campaign_form_submit($form, &$form_state) {
  
  $mailjet_campaign_asset = entity_ui_form_submit_build_entity($form, $form_state);
  $mailjet_campaign_asset->save(TRUE);
  
  $form_state['redirect'] = 'admin/mailjet/campaign';
}


function mailjet_campaign_sync() {
  
  // 
  $mj = new mailjet_integration();
  $params = array(
    //'id'  => '594472',
    'limit' => 10,
    //'orderby' => 'id ASC',
    //'start' => 0,
    //'status' => 'archived',
  );
  
  $response = $mj->messageCampaigns($params);
  
  if ($response && $response->status == 'OK') {
    $operations = array();
    foreach ($response->result as $key => $campaign) {
      $operations[] = array('mailjet_campaign_sync_op', array($campaign));
    }
    
    if (!empty($operations)) {
      $batch = array(
        'operations' => $operations,
        'finished' => 'mailjet_campaign_sync_op_finished',
        'title' => t('Processing synchronization Mailjet campaign'),
        'init_message' => t('Sync Mailjet campaign is starting.'),
        'progress_message' => t('Processed @current out of @total.'),
        'error_message' => t('Sync Mailjet campaign has encountered an error.'),
        'file' => drupal_get_path('module', 'mailjet_campaign') . '/inc/mailjet_campaign.admin.inc',
      );
      batch_set($batch);
      batch_process('admin/mailjet/campaign');
    }
    else {
      drupal_set_message(t('There is no campaign to import'), 'warning');
      drupal_goto('admin/mailjet/campaign');
    }
  }
  else {
    drupal_set_message(t('Can not synchronize Mailjet Campains : Network error'), 'error');
    drupal_goto('admin/mailjet/campaign');
  }
}

function mailjet_campaign_sync_op($campaign, &$context) {
  
  $mailjet_campaign = entity_load('mailjet_campaign', array($campaign->id));
  
  if (!empty($mailjet_campaign)) {
    // update entity
    $vars = get_object_vars($campaign);
    foreach ($vars as $key => $value) {
      $mailjet_campaign[$campaign->id]->$key = $value;
    }
    $mailjet_campaign[$campaign->id]->syncHtmlSource();
    $mailjet_campaign[$campaign->id]->getGlobalStatistics();
    $mailjet_campaign[$campaign->id]->save();
    $context['results']['update'] = (isset($context['results']['update'])) ? $context['results']['update'] + 1 : 1;
  }
  else {
    // create entity
    $data = array();
    $vars = get_object_vars($campaign);
    foreach ($vars as $key => $value) {
      $data[$key] = $value;
    }
    $data['cid'] = $data['id'];
    
    $mailjet_campaign = entity_create('mailjet_campaign', $data);
    $mailjet_campaign->syncHtmlSource();
    $mailjet_campaign->getGlobalStatistics();        
    if ($mailjet_campaign->save()) {
      $context['results']['insert'] = (isset($context['results']['insert'])) ? $context['results']['insert'] + 1 : 1;        
    }
    else {
      drupal_set_message(t('Can not create Mailjet campaign', 'error'));
    }
  }
}

function mailjet_campaign_sync_op_finished($success, $results, $operations) {
  if ($success) {
    if (isset($results['insert'])) {
      drupal_set_message(format_plural($results['insert'], '1 campaign have been created', '@count campaigns have been created'));
    }
    if (isset($results['update'])) {
      drupal_set_message(format_plural($results['update'], '1 campaign have been updated', '@count campaigns have been updated'));
    }
    drupal_set_message(t('Sync Mailjet campaign finished'));
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)));
    drupal_set_message($message, 'error');
  }
}