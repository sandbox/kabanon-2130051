<?php
/**
 * @file 
 * The Mailjet Campaign Entity class.
 */
class MailjetCampaignEntity extends Entity {

  protected $mailjetParams = array();
  protected $opProcess = TRUE;

  /**
   * Creates a new Mialjet Campaign Entity.
   *
   * @see entity_create()
   */
  public function __construct(array $values = array()) {
    parent::__construct($values, 'mailjet_campaign');
  }
  /**
   * Override this in order to implement a custom default URI.
   */
  protected function defaultUri() {
    return array('path' => 'mailjet_campaign/' . $this->identifier());
  }

  /**
   * Permanently saves the entity.
   *
   * @see entity_save()
   */
  public function save($update_on_mailjet = FALSE) {

    $op = (isset($this->is_new)) ? 'insert' : 'update';
    $this->updated_ts = time();
    // Update data with mailjet API.
    if ($update_on_mailjet) {
      // Redefine lang value.
      $this->lang = (isset($this->locale)) ? substr($this->locale, 0, 2) : '';

      if ($op == 'insert') {
        $this->created_ts = $this->updated_ts;
        $this->mailjetParams = array();
        // Check required params.
        $this->mailjetParams['method'] = 'POST';
        $this->defineMailjetParam('footer', 'footer', TRUE);
        $this->defineMailjetParam('sender_email', 'from', TRUE);
        $this->defineMailjetParam('sender_name', 'from_name', TRUE);
        $this->defineMailjetParam('lang', 'lang', TRUE);
        $this->defineMailjetParam('subject', 'subject', TRUE);
        // Additionnal params.
        $this->defineMailjetParam('list_id', 'list_id');
        $this->defineMailjetParam('reply_to', 'reply_to');
        $this->defineMailjetParam('sent_ts', 'sending_date');
        $this->defineMailjetParam('tpl_id', 'template_id');
        $this->defineMailjetParam('title', 'title');
        // If opProcess is TRUE, call mailjet API method.
        if ($this->opProcess) {
          $mj = new mailjet_integration();
          $response = $mj->messageCreateCampaign($this->mailjetParams);
          // Check response status.
          if (isset($response->status) && $response->status == 'OK') {
            $this->cid = $response->campaign->id;
            // Save entity.
            entity_get_controller($this->entityType)->save($this);
            // Update mailjetstatus.
            $mailjet_status = 'OK';
            // After saving campaign, we update it with mailjet api.
            $op = 'update';
          }
          else {
            // Error message.
            drupal_set_message(t('Error during campaign create method with mailjet API'), 'error');
          }
        }
      }
      // Update API call.
      if ($op == 'update') {
        $this->mailjetParams = array();
        // Check required params.
        $this->mailjetParams['method'] = 'POST';
        $this->defineMailjetParam('cid', 'id', TRUE);
        // Additionnal params.
        $this->defineMailjetParam('footer', 'footer');
        $this->defineMailjetParam('sender_email', 'from');
        $this->defineMailjetParam('sender_name', 'from_name');
        $this->defineMailjetParam('lang', 'lang');
        $this->defineMailjetParam('list_id', 'list_id');
        $this->defineMailjetParam('permalink', 'permalink');
        $this->defineMailjetParam('reply_to', 'reply_to');
        $this->defineMailjetParam('sent_ts', 'sending_date');
        $this->defineMailjetParam('subject', 'subject');
        $this->defineMailjetParam('tpl_id', 'template_id');
        $this->defineMailjetParam('title', 'title');

        if ($this->opProcess) {
          $mj = new mailjet_integration();
          $response = $mj->messageUpdateCampaign($this->mailjetParams);
          // Check response status.
          if (isset($response->status) && $response->status == 'OK') {
            $mailjet_status = $response->status;
            return entity_get_controller($this->entityType)->save($this);
          }
          else {
            // Error message.
            drupal_set_message(t('Error during campaign update method with mailjet API'), 'error');
          }
        }
      }
    }
    else {
      return entity_get_controller($this->entityType)->save($this);
    }
  }
  /**
   * Delete Mailjet Campaign.
   * On mailjet, campaign are not deleted, the status is set to empty.
   */
  public function delete() {
    $cid = $this->identifier();
    if (isset($cid)) {
      $this->mailjetParams = array();
      $this->mailjetParams['method'] = 'POST';
      $this->mailjetParams['id'] = $cid;
      $this->mailjetParams['status'] = '';
      if ($this->opProcess) {
        $mj = new mailjet_integration();
        $response = $mj->messageUpdateCampaign($this->mailjetParams);
        // Check response status.
        if (isset($response->status) && $response->status == 'OK') {
          $mailjet_status = $response->status;
        }
        $this->status = '';
        entity_get_controller($this->entityType)->save($this);
        // entity_get_controller($this->entityType)->delete(array($cid));
      }
    }
  }
  /**
   * Duplicate a campaign.
   */
  public function duplicate() {
    $this->mailjetParams = array();
    $this->mailjetParams['method'] = 'POST';
    // Required params.
    $this->defineMailjetParam('cid', 'id', TRUE);
    // Optional params.
    $this->defineMailjetParam('footer', 'footer');
    $this->defineMailjetParam('sender_email', 'from');
    $this->defineMailjetParam('sender_name', 'from_name');
    $this->defineMailjetParam('lang', 'lang');
    $this->defineMailjetParam('list_id', 'list_id');
    $this->defineMailjetParam('permalink', 'permalink');
    $this->defineMailjetParam('reply_to', 'reply_to');
    $this->defineMailjetParam('subject', 'subject');
    $this->defineMailjetParam('tpl_id', 'template_id');
    $this->defineMailjetParam('title', 'title');
    if ($this->opProcess) {
      $mj = new mailjet_integration();
      $response = $mj->messageDuplicatecampaign($this->mailjetParams);
      if (isset($response->status) && $response->status == 'OK') {
        $data = array();
        $vars = get_object_vars($this);
        foreach ($vars as $key => $value) {
          $data[$key] = $value;
        }
        $data['cid'] = $response->new_id;
        if ($new_campaign = entity_create('mailjet_campaign', $data)->save()) {
          drupal_set_message(t('The Mailjet campaign was succefully duplicated.'));
        }
        else {
          drupal_set_message(t('Error during Mailjet campaign entity on your site. Synchronize your campaign to fix this error.'), 'warning');
        }
      }
      else {
        // Error message.
        drupal_set_message(t('Error during campaign duplicate method with mailjet API'), 'error');
      }
    }
    drupal_goto('admin/mailjet/campaign');
  }
  /**
   * Save HTML source in entity and sync with Mailjet API.
   */
  public function saveHtmlSource() {
    $this->mailjetParams = array();
    $this->mailjetParams['method'] = 'POST';
    $this->defineMailjetParam('cid', 'id', TRUE);
    $this->defineMailjetParam('html_source', 'html', TRUE);
    $this->defineMailjetParam('txt_source', 'text');
    if ($this->opProcess) {
      $mj = new mailjet_integration();
      $response = $mj->messageHtmlCampaign($this->mailjetParams);
      if (isset($response->status) && $response->status == 'OK') {
        $this->save();
        return TRUE;
      }
      else {
        drupal_set_message(t('Can not save HTML source from Mailjet API', 'error'));
      }
    }
    return FALSE;
  }
  /**
   * Synchronize HTML Source with Mailjet data.
   */
  public function syncHtmlSource($save = FALSE) {
    $this->mailjetParams = array();
    $this->defineMailjetParam('cid', 'id', TRUE);
    if ($this->opProcess) {
      $mj = new mailjet_integration();
      $response = $mj->messageHtmlCampaign($this->mailjetParams);
      if (isset($response->status) && $response->status == 'OK') {
        $this->html_source = $response->html;
        if ($save) {
          $this->save();
          drupal_set_message(t('HTML content was successfully recovered from Mailjet API'));
        }
      }
      else {
        drupal_set_message(t('Can not get HTML source from Mailjet API', 'error'));
      }
    }
  }
  /**
   * Send campaign.
   */
  public function sendCampaign() {
    $this->mailjetParams = array();
    $this->mailjetParams['method'] = 'POST';
    $this->defineMailjetParam('cid', 'id', TRUE);
    if ($this->opProcess) {
      $mj = new mailjet_integration();
      $response = $mj->messageSendCampaign($this->mailjetParams);
      if (isset($response->status) && $response->status == 'OK') {
        return TRUE;
      }
      else {
        drupal_set_message(t('Can not send campaign from Mailjet API', 'error'));
      }
    }
    return FALSE;
  }
  /**
   * Send a campaign as a test to a given email address.
   * 
   * @param string $email The email address to send a test if test_addressproperties is not defined
   * 
   * @return bool Send status
   */
  public function sendTestCampaign($email = NULL) {
    $this->mailjetParams = array();
    $this->mailjetParams['method'] = 'POST';
    $this->defineMailjetParam('cid', 'id', TRUE);

    if (is_null($email)) {
      $this->defineMailjetParam('test_address', 'email', TRUE);
    }
    elseif (filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $this->mailjetParams['email'] = $email;
    }
    else {
      drupal_set_message(t('Test email address is not valid'), 'error');
      return FALSE;
    }
    if ($this->opProcess) {
      $mj = new mailjet_integration();
      $response = $mj->messageTestCampaign($this->mailjetParams);
      if (isset($response->status) && $response->status == 'OK') {
        return TRUE;
      }
      else {
        drupal_set_message(t('Can not send test email from Mailjet API', 'error'));
      }
    }
    return FALSE;
  }
  /**
   * Get global statistics from a specific message.
   */
  public function getGlobalStatistics($save = FALSE) {
    $this->mailjetParams = array();
    $this->defineMailjetParam('cid', 'id', TRUE);
    if ($this->opProcess) {
      $mj = new mailjet_integration();
      $response = $mj->messageStatistics($this->mailjetParams);
      if (isset($response->status) && $response->status == 'OK') {
        $this->bounce = (isset($response->result->bounce)) ? $response->result->bounce : NULL;
        $this->bounce_pct = (isset($response->result->bounce_pct)) ? $response->result->bounce_pct : NULL;
        $this->click = (isset($response->result->click)) ? $response->result->click : NULL;
        $this->click_pct = (isset($response->result->click_pct)) ? $response->result->click_pct : NULL;
        $this->open = (isset($response->result->open)) ? $response->result->open : NULL;
        $this->open_pct = (isset($response->result->open_pct)) ? $response->result->open_pct : NULL;
        $this->sent = (isset($response->result->sent)) ? $response->result->sent : NULL;
        $this->sent_pct = (isset($response->result->sent_pct)) ? $response->result->sent_pct : NULL;
        $this->spam = (isset($response->result->spam)) ? $response->result->spam : NULL;
        $this->spam_pct = (isset($response->result->spam_pct)) ? $response->result->spam_pct : NULL;
        $this->total = (isset($response->result->total)) ? $response->result->total : NULL;
        if ($save) {
          $this->save();
          drupal_set_message(t('Global statistics were successfully recovered from Mailjet API'));
        }
      }
      else {
        drupal_set_message(t('Can not get global statistics from Mailjet API'), 'error');
      }
    }
    return FALSE;
  }
  /**
   * Check required params before calling API mailjet method.
   * 
   * @param string $local_data Object properties to check
   * @param string $mailjet_key The mailjet parameter array key 
   */
  protected function defineMailjetParam($local_data, $mailjet_key, $required = FALSE) {
    if ($required && (!isset($this->{$local_data}) || empty($this->{$local_data}))) {
      $this->opProcess = FALSE;
    }
    if (isset($this->{$local_data}) && !empty($this->{$local_data})) {
      $this->mailjetParams[$mailjet_key] = $this->$local_data;
    }
  }
}
/**
 * Our custom controller for the mailjet_campaigns type.
 *
 * We're choosing to extend the controller provided by the entity module so that
 * we'll have full CRUD support for mailjet_campaignsentities.
 */
class MailjetCampaignEntityController extends EntityAPIController {

}

/**
 * Our custom controller for the admin ui.
 */
class MailjetCampaignEntityUIController extends EntityDefaultUIController {
  /**
   */
  public function overviewTable($conditions = array()) {
    $render = array(
      '#markup' => views_embed_view('mailjet_campaign_admin', 'default'),
    );
    return $render;
  }
}
