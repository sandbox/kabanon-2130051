<?php
/**
 * @file
 * Mailjet API implementation
 */
class mailjet_integration extends Mailjet {
  
  private $log = 0;
  /**
   * 
   */
  public function __construct() {
    $apiKey = variable_get('mailjet_integration_apikey');
    $secretKey = variable_get('mailjet_integration_secretkey');
    
    if (!is_null($apiKey) && !is_null($secretKey)) {
      parent::__construct($apiKey, $secretKey);
      $this->debug = variable_get('mailjet_integration_debug', 0);
      $this->log = variable_get('mailjet_integration_log', 0);
    }
    else {
      drupal_set_message(t('Mailjet Api & secret keys are not defined'));
      watchdog('Mailjet integration', 'Mailjet Api & secret keys are not defined', '', WATCHDOG_ERROR);
    }
  }
  /**
   * 
   * @param type $method
   * @param type $args
   */
  public function __call($method, $args) {
    $return  = parent::__call($method, $args);
    dsm($this);
    $this->log();
    if (isset($return->status) && $return->status == 'OK') {
      return $return;
    }
    else {
      if ($this->_response_code > '400') {
        drupal_set_message(t('Error during %method method Mailjet API call.', array('%method' => $method)), 'error');
      }
    }
  }
  /**
   * Log responce code in watchdog
   */
  private function log() {
    if (isset($this->_response_code)) {
      
      $message = '';
      
      $params = array(
        'method' => $this->_method,
        'request' => $this->_request,
        'call_url' => $this->call_url,
        'request_post' => $this->_request_post,
        'response_code' => $this->_response_code,
        'response' => $this->_response,
      );
      // severity
      if ($this->_response_code < '204') {
        $severity = WATCHDOG_INFO;
      }
      elseif ($this->_response_code < '400') {
        $severity = WATCHDOG_NOTICE;
      }
      else {
        $severity = WATCHDOG_ERROR;
      }
      
      if ($this->log > 0) {
        // Log success error messages
        if ($this->_response_code == '500') {
          $message = 'Mailjet API error : Internal Server Error';
        }
        if ($this->_response_code == '400') {
          $message = 'Mailjet API error : Bad Request';
        }
        if ($this->_response_code == '401') {
          $message = 'Mailjet API error : Unauthorized';
        }
        if ($this->_response_code == '404') {
          $message = 'Mailjet API error : Method Not Found';
        }
        if ($this->_response_code == '405') {
          $message = 'Mailjet API error : Method Not Allowed';
        }
      }
      if ($this->log > 1) {
        // Log notice messages
        if ($this->_response_code == '204') {
          $message = 'Mailjet API notice : No Content';
        }
        if ($this->_response_code == '304') {
          $message = 'Mailjet API notice : Not Modified';
        }
      }
      if ($this->log > 2) {
        // Log success messages
        if ($this->_response_code == '200') {
          $message = 'Mailjet API success : OK';
        }
        if ($this->_response_code == '201') {
          $message = 'Mailjet API success : Created';
        }
      }
      $log['message'] = $message;
      
      $variables = array(
        '%variables' => print_r($params, TRUE),
      );
      
      watchdog('Mailjet API ' . $this->_method, $message . ' <pre>%variables</pre>', $variables, $severity);
      
    }
  }
}