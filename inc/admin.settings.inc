<?php
/**
 * @file
 * Mailjet API implementation settings
 */
function mailjet_integration_admin_settings_form() {
  $form = array();
  $form['mailjet_integration_apikey'] = array(
    '#weight' => '0',
    '#required' => '1',
    '#type' => 'textfield',
    '#title' => t('Api key'),
    '#default_value' => variable_get('mailjet_integration_apikey', ''),
  );
  $form['mailjet_integration_secretkey'] = array(
    '#weight' => '10',
    '#required' => '1',
    '#type' => 'textfield',
    '#title' => t('Secret key'),
    '#default_value' => variable_get('mailjet_integration_secretkey', ''),
  );
  $form['mailjet_integration_debug'] = array(
    '#weight' => '20',
    '#required' => '1',
    '#type' => 'select',
    '#title' => t('Debug mode'),
    '#default_value' => variable_get('mailjet_integration_debug', '0'),
    '#options' => array(
      '0' => 'none',    
      '1' => 'errors only',
      '2' => 'all',
    ),
  );
  $form['mailjet_integration_log'] = array(
    '#weight' => '20',
    '#required' => '1',
    '#type' => 'select',
    '#title' => t('Mailjet log option'),
    '#default_value' => variable_get('mailjet_integration_log'),
    '#options' => array(
      '0' => 'none',    
      '1' => 'errors only',
      '2' => 'notices & errors',
      '3' => 'all',
    ),
  );
  return system_settings_form($form);
}

function mailjet_integration_admin_settings_form_validate($form, &$form_state) {
  // Test API & secret key
  $mj = new Mailjet();
  $mj->apiKey = $form_state['values']['mailjet_integration_apikey'];
  $mj->secretKey = $form_state['values']['mailjet_integration_secretkey'];
  $mj->debug = 0;
  $mj_user_info = $mj->userInfos();
  
  if (!is_object($mj_user_info)) {
    form_set_error('mailjet_integration_apikey', t('Can not connect to mailjet with this api and secret keys'));
    form_set_error('mailjet_integration_secretkey'); 
  }
}